# DNA Mutation
This application allows scientists to send a dna chain through the web and the shall receive a response if the DNA has been mutated or not.
It also allows the scientists to keep track of all the different dna sequences and get statistics about their mutation. 

# Requirements
The project can be executed both as a standalone server (on premise or EC2 instance or similar) or as a serverless service using API Gateway + lambdas on AWS. 

 - Go latest version [Install Docs](https://golang.org/doc/install)
 - Mongo DB [Install Docs](https://www.mongodb.com/try/download/community) or Cloud [Mongo Atlas](https://account.mongodb.com/account/login?nds=true)
 - If deploying to AWS serverless, you will need an AWS Account [AWS Account](https://aws.amazon.com/es/console/) and a 
   Serverless Account [Serverless Docs](https://www.serverless.com/framework/docs/getting-started/)

# Install Instructions

 1) Clone the project in your $GOROOT folder

```
git clone https://gitlab.com/snavarro89/guros.git
```

2) You can run dependencies now or they will be brought when building the application

```
cd guros
go get
```

3) Init your db (Run the command with mongo shell)

```
cd scripts
mongo < initdb.js
```
4) Review your environment file (development test found on git folder), you will have:
DB_URL = URL to mongo database, most common on local: localhost:27017
DB_USER = Username allowed to connect to mongo, leave empty if no user name to secure the database
DB_PASSWORD = Password allowed to connect to mongo, leave empty if no password to secure the database
DB_NAME=By default 'guros' change if you created a different db
ENV= By default 'dev' when building for production change to 'production'
JWT_SECRET= 32 long secret Key, ideally this key will be generated on signing with a combination of requesting user+password+constant.
TOKEN_DURATION= Format as '100h' = 100 hours, this is the time when the token will be expired.

# On Premise Deployment (Currently only working as localhost mongo)

1) From the guros folders

```
cd mux
go build -o bin/mux
bin/mux
```

2) This should start your server, you can now reach your API service

# Serverless Deployment

1) From the guros folder

```
scripts/build.sh
```
2) Add a new file under environment called .env.production (this file will not be commited to git as it is being ignored)
3) Copy variables from .env
4) DB_URL FORMAT 
   myurl.vl7xc
   DO NOT ADD mongo+srv:// prefix, only as indicated for program to be able to connect to DB.
5) After configuring your AWS and Serverless accounts
6) Add a aws profile to ~/.aws/credentials called [aws-guros-test] TODO: Add env variable to choose whatever profile you want.

After deploying you will have following routes

http://localhost:3001/dna
http://localhost:3001/stats
http://localhost:3001/authenticate

or on serverless

http://DOMAIN/production/dna
http://DOMAIN/production/stats
http://DOMAIN/production/authenticate

To access both /dna and /stats api's you need to call first authenticate to get an api key, that key needs to be send as an Authorization Token
BEARER TOKEN_PROVIDED

Authenticate Api JSON Format (The init db has a user with following credentials allowed)
```
{
    "email": "demo@demo.com",
    "password": "demo"
}
```


DNA Create (Has Mutation) JSON Format
POST
```
{
    "dna": ["AGTCGT", "GAATTA", "AGACAA", "AAACCC", "ATTGTG", "ATCAGT"]
}
```


package api

import (
	models "guros/common/models"
	"os"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"

	"context"
)

type DnaModel struct {
	DB *mongo.Client
}

func (m DnaModel) Stats() ([]models.Stat, error) {

	var (
		err   error
		stats []models.Stat
	)

	mutationGroupState := bson.D{{"$group", bson.D{
		{"_id", "$hasMutation"},
		{"count", bson.D{{"$sum", 1}}},
	}}}

	cursor, err := m.DB.Database(os.Getenv("DB_NAME")).Collection("dna").Aggregate(context.TODO(), mongo.Pipeline{mutationGroupState})

	if err != nil {
		return nil, err
	}
	err = cursor.All(context.TODO(), &stats)
	cursor.Close(context.TODO())

	return stats, err

}

func (m DnaModel) Create(dna models.Dna) (models.Dna, error) {

	var (
		err error
	)

	_, err = m.DB.Database(os.Getenv("DB_NAME")).Collection("dna").InsertOne(context.TODO(), dna)
	if err != nil {
		return dna, err
	}

	return dna, nil

}

func (m DnaModel) Get(chain []string) (models.Dna, error) {

	var (
		err error
		dna models.Dna
	)

	filter := bson.M{
		"chain": chain,
	}
	err = m.DB.Database(os.Getenv("DB_NAME")).Collection("dna").FindOne(context.TODO(), filter).Decode(&dna)
	if err != nil {
		return dna, err
	}

	return dna, nil

}

package api

import (
	"context"
	"fmt"
	models "guros/common/models"
	"os"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type UserModel struct {
	DB *mongo.Client
}

func (m UserModel) GetUserByEmail(email string) (models.User, error) {
	var (
		result models.User
		err    error
	)

	err = m.DB.Database(os.Getenv("DB_NAME")).Collection("user").FindOne(context.TODO(), bson.M{}).Decode(&result)
	if err != nil {
		return result, fmt.Errorf("email not found: %s", email)
	}

	return result, nil
}

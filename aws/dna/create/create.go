package main

import (
	Functions "guros/functions"
	H "guros/functions/dna/create"
)

func main() {
	env := Functions.Init()
	H.Env(env)
	H.Aws()
}

package main

import (
	Functions "guros/functions"
	H "guros/functions/dna/stats"
)

func main() {
	env := Functions.Init()
	H.Env(env)
	H.Aws()
}

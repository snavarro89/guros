package main

import (
	Functions "guros/functions"
	H "guros/functions/user/authenticate"
)

func main() {
	env := Functions.Init()
	H.Env(env)
	H.Aws()
}

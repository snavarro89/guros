package common

import "go.mongodb.org/mongo-driver/bson/primitive"

type Dna struct {
	ID          primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Chain       []string           `json:"chain" bson:"chain"`
	HasMutation bool               `json:"hasMutation" bson:"hasMutation"`
}

type Stat struct {
	ID    bool  `bson:"_id"`
	Count int32 `bson:"count"`
}

type User struct {
	ID       primitive.ObjectID `json:"_id" bson:"_id"`
	Email    string             `json:"email" bson:"email"`
	Password string             `json:"password" bson:"password,omitempty"`
	Active   bool               `json:"active" bson:"active"`
}

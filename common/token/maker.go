package token

import "time"

type Maker interface {
	CreateToken(duration time.Duration) (string, error)
	VerifyToken(token string) (*Payload, error)
}

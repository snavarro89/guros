package utils

import (
	"strings"
)

func searchWord(matrix [][]string, solution [][]int, path int, k int) bool {
	n := len(matrix)
	for i := 0; i < k; i++ {
		for j := 0; j < n; j++ {
			if search(matrix, solution, path, i, j, 0, n, k, "", matrix[i][j]) {
				return true
			}
		}
	}
	return false
}

func search(matrix [][]string, solution [][]int, path int, row int, col int, index int, n int, k int, direction string, letter string) bool {

	if solution[row][col] != 0 || letter != matrix[row][col] {
		return false
	}

	if index == k-1 {
		path++
		solution[row][col] = path
		return true
	}

	path++
	solution[row][col] = path

	//Vertical
	if row+1 < n && (direction == "" || direction == "VD") && search(matrix, solution, path, row+1, col, index+1, n, k, "VD", letter) {
		return true
	}
	//Horizontally
	if col+1 < n && (direction == "" || direction == "HR") && search(matrix, solution, path, row, col+1, index+1, n, k, "HR", letter) {
		return true
	}
	//Diagonally
	if row+1 < n && col-1 >= 0 && (direction == "" || direction == "DL") && search(matrix, solution, path, row+1, col-1, index+1, n, k, "DL", letter) {
		return true
	}
	if row+1 < n && col+1 < n && (direction == "" || direction == "DR") && search(matrix, solution, path, row+1, col+1, index+1, n, k, "DR", letter) {
		return true
	}

	solution[row][col] = 0
	path--
	return false
}

func HasMutation(dna []string) bool {
	matrixDna, solution, error := getMatrix(dna)
	if error {
		return false
	}
	if searchWord(matrixDna, solution, 0, 4) {
		return true
	}
	return false
}

func getMatrix(dna []string) ([][]string, [][]int, bool) {
	dp := make([][]string, len(dna))
	solution := make([][]int, len(dna))
	for i, value := range dna {
		line := strings.Split(value, "")
		if len(dna) != len(line) {
			return nil, nil, true
		}
		dp[i] = make([]string, len(line))
		solution[i] = make([]int, len(line))
		for j, _ := range line {
			solution[i][j] = 0
		}
		dp[i] = line
	}
	return dp, solution, false
}

package utils

import (
	"testing"
)

func TestEmptyArray(t *testing.T) {
	matrix := []string{}
	value := HasMutation(matrix)
	if value {
		t.Fatalf(`HasMutation([]{}) = Empty array should return false, received: %t`, value)
	}
}

func TestInvalidMatrix(t *testing.T) {
	matrix := []string{"ATCGGT", "ATT"}
	value := HasMutation(matrix)
	if value {
		t.Fatalf(`HasMutation([]{"ATCGGT", "ATT"}) = Invalid Matrix should return false, received: %t`, value)
	}
}

func TestNoMutation(t *testing.T) {
	matrix := []string{"AGTCGT", "AAATTA", "AGACAA", "TAACCC", "ATTGTG", "ATCAGT"}
	value := HasMutation(matrix)
	if value {
		t.Fatalf(`HasMutation([]{"AGTCGT", "AAATTA", "AGACAA", "TAACCC", "ATTGTG", "ATCAGT"}) = Should return false, received: %t`, value)
	}
}

func TestMutationHorizontalFirstRow(t *testing.T) {
	matrix := []string{"AAAAGT", "AAATTA", "AGACAA", "AAACCC", "ATTGTG", "ATCAGT"}
	value := HasMutation(matrix)
	if !value {
		t.Fatalf(`HasMutation([]{"AAAAGT", "AAATTA", "AGACAA", "AAACCC", "ATTGTG", "ATCAGT"}) = Should return true, received: %t`, value)
	}
}

func TestMutationHorizontalLastRow(t *testing.T) {
	matrix := []string{"AGTCGT", "TAATTA", "AGACAA", "AAACCA", "ATTGGA", "ATCAGA"}
	value := HasMutation(matrix)
	if !value {
		t.Fatalf(`HasMutation([]{"AGTCGT", "TAATTA", "AGACAA", "TAACCC", "ATTGTG", "ATCAGT"}) = Should return true, received: %t`, value)
	}
}

func TestMutationVerticalFirstColumn(t *testing.T) {
	matrix := []string{"AGTCGT", "AAATTA", "AAACAA", "ATACCA", "ATTGGA", "ATCAGA"}
	value := HasMutation(matrix)
	if !value {
		t.Fatalf(`HasMutation([]{"AGTCGT", "AAATTA", "AAACAA", "ATACCA", "ATTGGA", "ATCAGA"}) = Should return true, received: %t`, value)
	}
}

func TestMutationVerticalLastColumn(t *testing.T) {
	matrix := []string{"AGTCGT", "CAATTA", "CAACAA", "ATACCA", "ATTGGA", "ATCAGA"}
	value := HasMutation(matrix)
	if !value {
		t.Fatalf(`HasMutation([]{"AGTCGT", "CAATTA", "CAACAA", "ATACCA", "ATTGGA", "ATCAGA"}) = Should return true, received: %t`, value)
	}
}

func TestMutationDiagonalLeft(t *testing.T) {
	matrix := []string{"AGTCGT", "CAATTA", "CAATAC", "ATTCCA", "ATTGGA", "ATCAGA"}
	value := HasMutation(matrix)
	if !value {
		t.Fatalf(`HasMutation([]{"AGTCGT", "CAATTA", "CAATAC", "ATTCCA", "ATTGGA", "ATCAGA"}) = Should return true, received: %t`, value)
	}
}

func TestMutationDiagonalRight(t *testing.T) {
	matrix := []string{"AGTCGT", "AAAGTA", "AGGCAA", "TGACCC", "ATTGTG", "ATCAGT"}
	value := HasMutation(matrix)
	if !value {
		t.Fatalf(`HasMutation([]{"AGTCGT", "AAAGTA", "AGGCAA", "TGACCC", "ATTGTG", "ATCAGT"}) = Should return true, received: %t`, value)
	}
}

package utils

import "golang.org/x/crypto/bcrypt"

func CheckPassword(dbPass, reqPass string) error {
	errPwd := bcrypt.CompareHashAndPassword([]byte(dbPass), []byte(reqPass))

	return errPwd
}

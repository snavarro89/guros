package db

import (
	"context"
	"os"
	"strconv"
	"sync"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

/* Used to create a singleton object of MongoDB client.
Initialized and exposed through  GetMongoClient().*/
var clientInstance *mongo.Client

//Used during creation of singleton client object in GetMongoClient().
var clientInstanceError error

//Used to execute client creation procedure only once.
var mongoOnce sync.Once

//I have used below constants just to hold required database config's.
var (
	dbUrl                  = ""
	dbName                 = ""
	dbUser                 = ""
	dbPwd                  = ""
	dbTimeout        int64 = 5
	connectionString       = ""
)

func Init() {
	dbUrl = os.Getenv("DB_URL")
	dbName = os.Getenv("DB_NAME")
	dbUser = os.Getenv("DB_USER")
	dbPwd = os.Getenv("DB_PWD")
	dbTimeout, _ = strconv.ParseInt(os.Getenv("DB_TIMEOUT"), 10, 64)
	if os.Getenv("ENV") == "dev" {
		connectionString = "mongodb://" + dbUrl
	} else {
		connectionString = "mongodb+srv://" + dbUser + ":" + dbPwd + "@" + dbUrl + ".mongodb.net/" + dbName + "?retryWrites=true&w=majority"
	}
}

//GetMongoClient - Return mongodb connection to work with
func GetMongoClient() (*mongo.Client, error) {
	//Perform connection creation operation only once.
	mongoOnce.Do(func() {
		//Set the context
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(dbTimeout)*time.Second)
		defer cancel()
		// Set client options
		clientOptions := options.Client().ApplyURI(connectionString)
		// Connect to MongoDB
		client, err := mongo.Connect(ctx, clientOptions)
		if err != nil {
			clientInstanceError = err
		}
		// Check the connection
		err = client.Ping(context.TODO(), nil)
		if err != nil {
			clientInstanceError = err
		}
		clientInstance = client
	})
	return clientInstance, clientInstanceError
}

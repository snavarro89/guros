package functions

import (
	"fmt"
	dna "guros/api/dna"
	user "guros/api/user"
	models "guros/common/models"
	"guros/db"

	"go.mongodb.org/mongo-driver/mongo"
)

var Mongo *mongo.Client

func setMongoClient() {
	db.Init()
	mongo, err := db.GetMongoClient()
	if err != nil {
		fmt.Printf("error to connect with DB, %s", err.Error())
	}

	Mongo = mongo
}

type Dna interface {
	Create(models.Dna) (models.Dna, error)
	Stats() ([]models.Stat, error)
	Get([]string) (models.Dna, error)
}

type User interface {
	GetUserByEmail(string) (models.User, error)
}

type Env struct {
	Dna
	User
}

func Init() Env {
	setMongoClient()
	env := Env{
		Dna:  dna.DnaModel{DB: Mongo},
		User: user.UserModel{DB: Mongo},
	}
	return env
}

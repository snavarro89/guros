package functions

import (
	"encoding/json"
	M "guros/common/models"
	U "guros/common/utils"
	F "guros/functions"
	H "guros/handler"
	"net/http"
	"strings"

	"github.com/aws/aws-lambda-go/lambda"
)

var env F.Env

func Env(e F.Env) {
	env = e
}

func Mux(w http.ResponseWriter, r *http.Request) {
	m := H.MuxResponse{
		Handle: func(params H.Parameters) (interface{}, int) {
			return handler(params)
		},
		SkipJWT: false,
	}
	m.Callback(w, r)
}

func Aws() {
	m := H.AWSResponse{
		Handle: func(params H.Parameters) (interface{}, int) {
			return handler(params)
		},
		SkipJWT: false,
	}
	lambda.Start(m.Callback)
}

type DnaRequest struct {
	Dna []string
}

func handler(params H.Parameters) (interface{}, int) {

	var dnaReq DnaRequest
	err := json.Unmarshal(params.Body, &dnaReq)
	if err != nil {
		errResponse := map[string]interface{}{
			"error": true,
			"desc":  "Please review api specs for correct format",
			"code":  "1000",
		}
		return errResponse, http.StatusBadRequest
	}
	var dna M.Dna

	dna, err = env.Dna.Get(dnaReq.Dna)
	response := map[string]interface{}{
		"newRecord":   true,
		"hasMutation": false,
	}
	if err != nil {
		mongoError := err.Error()
		if !strings.Contains(mongoError, "no documents") {
			errResponse := map[string]interface{}{
				"error": true,
				"desc":  "Please contact admin",
				"code":  "1001",
			}
			return errResponse, http.StatusInternalServerError
		}
		//If the resource was not found then we shold find mutation and save to database
		dna.Chain = dnaReq.Dna
		dna.HasMutation = U.HasMutation(dnaReq.Dna)

		_, err = env.Dna.Create(dna)
		if err != nil {
			errResponse := map[string]interface{}{
				"error": true,
				"desc":  "Please contact admin",
				"code":  "1003",
			}
			return errResponse, http.StatusInternalServerError
		}
	} else {
		response["newRecord"] = false
	}

	if dna.HasMutation {
		response["hasMutation"] = true
		return response, http.StatusOK
	}

	return response, http.StatusForbidden
}

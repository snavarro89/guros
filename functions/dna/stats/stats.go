package functions

import (
	M "guros/common/models"
	F "guros/functions"
	H "guros/handler"
	"net/http"

	"github.com/aws/aws-lambda-go/lambda"
)

var env F.Env

func Env(e F.Env) {
	env = e
}

func Mux(w http.ResponseWriter, r *http.Request) {
	m := H.MuxResponse{
		Handle: func(params H.Parameters) (interface{}, int) {
			return handler(params)
		},
		SkipJWT: false,
	}
	m.Callback(w, r)
}

func Aws() {
	m := H.AWSResponse{
		Handle: func(params H.Parameters) (interface{}, int) {
			return handler(params)
		},
		SkipJWT: false,
	}
	lambda.Start(m.Callback)
}

type StatsResponse struct {
	CountMutations   int
	CountNoMutations int
	Ratio            float32
}

func handler(params H.Parameters) (interface{}, int) {

	var result []M.Stat
	result, err := env.Dna.Stats()
	if err != nil {
		errResponse := map[string]interface{}{
			"error": true,
			"desc":  "Please contact admin",
			"code":  "1001",
		}
		return errResponse, http.StatusInternalServerError
	}

	response := StatsResponse{
		CountMutations:   0,
		CountNoMutations: 0,
		Ratio:            0.0,
	}

	for _, v := range result {
		if v.ID {
			response.CountMutations = int(v.Count)
		} else {
			response.CountNoMutations = int(v.Count)
		}
	}
	response.Ratio = float32(response.CountMutations) / float32(response.CountNoMutations)

	return response, http.StatusOK
}

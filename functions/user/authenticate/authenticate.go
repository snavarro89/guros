package functions

import (
	"encoding/json"
	"guros/common/token"
	"guros/common/utils"
	F "guros/functions"
	H "guros/handler"
	"net/http"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
)

var env F.Env

func Env(e F.Env) {
	env = e
}

func Mux(w http.ResponseWriter, r *http.Request) {
	m := H.MuxResponse{
		Handle: func(params H.Parameters) (interface{}, int) {
			return handler(params)
		},
		SkipJWT: true,
	}
	m.Callback(w, r)
}

func Aws() {
	r := H.AWSResponse{
		Handle: func(params H.Parameters) (interface{}, int) {
			return handler(params)
		},
		SkipJWT: true,
	}
	lambda.Start(r.Callback)
}

type LoginUserRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func handler(params H.Parameters) (interface{}, int) {
	var userCredentials LoginUserRequest
	errResponse := make(map[string]interface{})

	err := json.Unmarshal(params.Body, &userCredentials)
	if err != nil {
		errResponse = map[string]interface{}{
			"error": true,
			"desc":  "Please review api specs for correct format",
			"code":  "1000",
		}
		return errResponse, http.StatusBadRequest
	}

	makerToken, err := token.NewJWTMaker(os.Getenv("JWT_SECRET"))
	if err != nil {
		errResponse = map[string]interface{}{
			"error": true,
			"desc":  "Server Error, please contact Admin",
			"code":  "1001",
		}
		return errResponse, http.StatusInternalServerError
	}

	userDB, err := env.User.GetUserByEmail(userCredentials.Email)
	if err != nil {
		errResponse = map[string]interface{}{
			"error": true,
			"desc":  "Unauthorized access",
			"code":  "403",
		}
		return errResponse, http.StatusForbidden
	}

	errPass := utils.CheckPassword(userDB.Password, userCredentials.Password)

	if errPass != nil {
		errResponse = map[string]interface{}{
			"error": true,
			"desc":  "Unauthorized access",
			"code":  "403",
		}
		return errResponse, http.StatusForbidden
	}

	tokenExpiredTime, err := time.ParseDuration(os.Getenv("TOKEN_DURATION"))

	if err != nil {
		errResponse = map[string]interface{}{
			"error": true,
			"desc":  "Server Error, please contact Admin",
			"code":  "1003",
		}
		return errResponse, http.StatusInternalServerError
	}

	token, err := makerToken.CreateToken(tokenExpiredTime)
	if err != nil {
		errResponse = map[string]interface{}{
			"error": true,
			"desc":  "Server Error, please contact Admin",
			"code":  "1004",
		}
		return errResponse, http.StatusInternalServerError
	}

	response := make(map[string]interface{})
	response["error"] = false
	response["accessToken"] = token

	return response, http.StatusOK
}

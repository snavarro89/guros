module guros

go 1.14

require (
	github.com/aws/aws-lambda-go v1.24.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	go.mongodb.org/mongo-driver v1.5.3
	golang.org/x/crypto v0.0.0-20200302210943-78000ba7a073
)

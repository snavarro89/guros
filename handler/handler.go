package handler

import (
	"encoding/json"
	"guros/common/token"
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"github.com/aws/aws-lambda-go/events"
)

type Handle func(params Parameters) (interface{}, int)

type AWSResponse struct {
	Handle  Handle
	SkipJWT bool
}

type MuxResponse struct {
	Handle  Handle
	SkipJWT bool
}

type Parameters struct {
	Body []byte
}

func (r AWSResponse) Callback(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	var payload *token.Payload
	headers := map[string]string{"Access-Control-Allow-Origin": "*", "Access-Control-Allow-Methods": "DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT", "Access-Control-Allow-Headers": "Access-Control-Allow-Headers,Access-Control-Allow-Methods,Access-Control-Allow-Origin,Content-Type,Authorization,X-Amz-Date,X-Api-Key,X-Amz-Security-Token"}

	if !r.SkipJWT {
		authorized := false
		tokenString := strings.TrimPrefix(req.Headers["Authorization"], "Bearer ")
		maker, err := token.NewJWTMaker(os.Getenv("JWT_SECRET"))
		if err == nil {

			payload, err = maker.VerifyToken(tokenString)

			if err == nil && payload != nil {
				authorized = true
			}
		}

		if !authorized {
			errResponse := map[string]interface{}{
				"error": true,
				"desc":  "Unauthorized access",
				"code":  "403",
			}
			jsonString, _ := json.Marshal(errResponse)
			return events.APIGatewayProxyResponse{Body: string(jsonString), StatusCode: http.StatusForbidden, Headers: headers}, nil
		}
	}

	result, status := r.Handle(Parameters{
		Body: []byte(req.Body),
	})

	jsonString, _ := json.Marshal(result)
	return events.APIGatewayProxyResponse{Body: string(jsonString), StatusCode: status, Headers: headers}, nil
}

func (m MuxResponse) Callback(w http.ResponseWriter, r *http.Request) {
	var payload *token.Payload
	if !m.SkipJWT {
		var header = strings.TrimSpace(r.Header.Get("Authorization"))
		authorized := false
		if header != "" {
			tokenString := strings.TrimPrefix(header, "Bearer ")

			maker, err := token.NewJWTMaker(os.Getenv("JWT_SECRET"))
			if err == nil {
				payload, err = maker.VerifyToken(tokenString)
				if err == nil && payload != nil {
					authorized = true
				}
			}

		}

		if !authorized {
			w.WriteHeader(http.StatusForbidden)
			errResponse := map[string]interface{}{
				"error": true,
				"desc":  "Unauthorized access",
				"code":  "403",
			}
			json.NewEncoder(w).Encode(errResponse)
			return
		}
	}

	body, _ := ioutil.ReadAll(r.Body)
	result, status := m.Handle(Parameters{
		Body: body,
	})
	if status != http.StatusOK {
		w.WriteHeader(status)
	}

	json.NewEncoder(w).Encode(result)
}

type Response interface {
	Callback() (map[string]interface{}, int)
}

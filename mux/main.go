package main

import (
	Functions "guros/functions"
	DnaCreate "guros/functions/dna/create"
	DnaStats "guros/functions/dna/stats"
	UserAuthenticate "guros/functions/user/authenticate"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
)

func init() {
	if err := godotenv.Load("../environment/.env"); err != nil {
		log.Print("No .env file found")
	}
}

func main() {

	headersOk := handlers.AllowedHeaders([]string{"Accept", "Accept-Language", "Content-Type", "Content-Language", "Origin", "Access-Control-Allow-Headers", "Access-Control-Allow-Methods", "Authorization", "X-Auth-Token", "Access-Control-Allow-Origin"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "POST", "PATCH", "PUT", "DELETE", "OPTIONS"})
	env := Functions.Init()
	DnaCreate.Env(env)
	DnaStats.Env(env)
	UserAuthenticate.Env(env)
	router := mux.NewRouter().StrictSlash(true)

	//DNA Routes - Require Authentication
	router.HandleFunc("/dna", DnaCreate.Mux).Methods("POST")
	router.HandleFunc("/stats", DnaStats.Mux).Methods("GET")

	//User Routes - Authenticate and get token for authorization
	router.HandleFunc("/authenticate", UserAuthenticate.Mux).Methods("POST")
	log.Fatal(http.ListenAndServe(":3001", handlers.CORS(originsOk, headersOk, methodsOk)(router)))

}
